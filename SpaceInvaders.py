import time
import signal
#import GetInput
from Classes import _Getch
import shutil

from random import randint
from Classes import Board
from Classes import Spaceship
from Classes import Missile
from Classes import Alien
from Classes import Bcolors
from Classes import Debug
from Classes import Explosion


class Engine(Board, Spaceship, Missile, Alien):
    '''game engine that inherits from classes
       Board, Spaceship, Missile, Alien'''
    def __init__(self):
        self.screen_size = shutil.get_terminal_size()
        print("ENGINE INIT:screen size: ", self.screen_size[0], " ", self.screen_size[1])
        #self.engine_board_width = self.screen_size[0]
        #self.engine_board_height = self.screen_size[1] - 4 #Leave bottom x rows as status space 
        self.engine_board_width = 20
        if self.screen_size[1] > 25:
            self.engine_board_height = 25
        else:
            self.engine_board_height = self.screen_size[1]

        self.alien_ttl = 100 #seconds for time-to-live
        self.board_array_size = int(self.engine_board_height * self.engine_board_width)
        #We position the spaceship on the bottom row, minus half of the bottom row's width

        self.spaceship_initial_position = self.board_array_size - int(self.engine_board_width/2)
        #We here define the various object chars
        self.ship_ch = Bcolors.WARNING + '^' + Bcolors.ENDC
        self.alien1_ch = Bcolors.OKGREEN + 'A' + Bcolors.ENDC
        self.alien2_ch = Bcolors.OKCYAN + 'A' + Bcolors.ENDC
        self.explosion_ch = Bcolors.BOLD + '*' + Bcolors.ENDC
        self.space_ch = ' '
        self.board = Board(self.engine_board_width, self.engine_board_height)
        self.myShip = Spaceship(self.ship_ch, self.spaceship_initial_position, self.board, self.engine_board_width, self.engine_board_height)
        self.miss1 = []
        self.miss2 = []
        self.aliens = []
        self.explosions = []
        self.score = 0
        self.start = time.time()
        #self.getch = GetInput._getChUnix()
        self.getch = _Getch() #makes this OS independent
        self.total_aliens = 0
        self.total_missiles = 0
        self.alien_direction = 1 #Let's start by moving to the right
        self.aliens_hit = 0
        Debug.debug_string("*")
    #method that returns the alien 
    #object at a given position 
    #when a missile collides with 
    #the alien at that position
    def findalien(self, pos):
        for i in self.aliens:
            Debug.debug_string("alien_obj pos: " + str(i.pos), "missile_pos: " + str(pos))
            if i.pos == pos:
                return i
                Debug.debug_string("found alien: " + str(i))

    #method to update the gameboard
    def updateboard(self):
        delmiss = []
        delaliens = []
        delexplosions = []
        
        #clear all explosions
        for i in self.explosions:
            self.board.grid[i.pos] = self.space_ch #make it plane space again
            self.explosions.remove(i) #remove it from our list of things
        
        #update positions of missiles
        for i in self.miss1:

            if time.time() - i.time >= 1:
                
                if self.board.grid[i.pos] == i.ch:
                    self.board.grid[i.pos] = self.space_ch
                i.pos -= i.jump

                if i.pos < 0:
                    delmiss.append(i)
                    continue

                if self.board.grid[i.pos] == self.alien1_ch:
                    Debug.debug_string("i: " + self.board.grid[i.pos], "alien_str: " + self.alien1_ch, "i.pos: " + str(i.pos)," missile_pos: " + str(i.pos))
                    Debug.debug_grid(self.board.grid, self.engine_board_height, self.engine_board_width)
                    delmiss.append(i)
                    self.score += 10
                    self.aliens_hit += 1
                    alien_obj = self.findalien(i.pos)
                    delaliens.append(alien_obj)
                    #Debug.debug_string("alien_obj: " + str(alien_obj), str(delaliens))
                    #make explosion
                    self.board.grid[i.pos] = self.explosion_ch
                    Debug.debug_string("i: " + self.board.grid[i.pos], "expl_str: " + self.explosion_ch, "i.pos: " + str(i.pos)," missile_pos: " + str(i.pos))                    
                    new_expl = Explosion(self.explosion_ch, i.pos, time.time())
                    self.explosions.append(new_expl) #Append the explosion to the list
                    continue

                else:
                    i.time = time.time()
                    self.board.grid[i.pos] = i.ch

        #let's remove any dead missiles
        for i in delmiss:
            self.miss1.remove(i)

        delmiss = []
        del delmiss
        
        # Lets delete any hit aliens
        for i in delaliens:
            Debug.debug_string("deleting alien: " + str(i))
            try:
                self.aliens.remove(i)
                self.total_aliens -= 1 #reduce the headcount
            except ValueError:
                pass
        
        delaliens = []
        del delaliens
        

        
        #Let's see if any aliens are on the left or the right boundary
        alien_pos = []
        left_edge_mod_string = ""
        right_edge_mod_string = ""
        alien_jump = False
        
        #remove aliens that have been
        #on the board for alient_ttl seconds
        for i in reversed(self.aliens): #Iterate through the list backwards
            #Let's see if any aliens are on the left or the right boundary
            Debug.debug_string("aliens.pos: " + str(i.pos), "grid.pod: " + str(self.board.grid[i.pos]))                    
            # alien_pos.append(i.pos)
            #Are there any aliens near one edge?
            left_edge_mod = i.pos % self.engine_board_width
            right_edge_mod = i.pos % self.engine_board_width
            #Debug.debug_string(left_edge_mod_string, "i.pos: " + str(i.pos), "board_width: " + str(self.engine_board_width))
            if left_edge_mod <= 1:
                #we are here because we're close to the left edge.
                #We have to reverse direction of the alien movement
                self.alien_direction = 1
                #And we have to move all the aliens down
                alien_jump = True
            
            elif right_edge_mod >= self.engine_board_width - 1:
                #we are here because one we're close to the right edge
                #We have to reverse direction of the alien movement
                self.alien_direction = -1
                #And we have to move all the aliens down
                alien_jump = True
                
            
            if alien_jump:
                if i.pos + self.engine_board_width < len(self.board.grid):
                    self.board.grid[i.pos + self.engine_board_width] = self.board.grid[i.pos]
                    #And we clear the old cell
                    self.board.grid[i.pos] = self.space_ch
                    i.pos = i.pos + self.engine_board_width
                
            #Finally, let's move this alien in the right direction
            self.board.grid[i.pos] = self.space_ch #firstly, make the current cell, space
            i.pos = i.pos + self.alien_direction
            self.board.grid[i.pos] = self.alien1_ch #make the new cell show the alien
            
#            Debug.debug_string('alien direction: ' + str(self.alien_direction), "i.pos: " + str(i.pos), "missles: " + str(self.total_missiles) + " - aliens: " + str(self.total_aliens) )
#            Debug.debug_grid(self.board.grid, self.engine_board_height, self.engine_board_width)
            
            if time.time() - i.time >= self.alien_ttl:
                delaliens.append(i)     
                self.board.grid[i.pos] = self.space_ch
                #For each alien we delete this way, reduce score by 5 points for player tardiness
                self.score -= 5
                

        #spawning three aliens randomly
        #after every X seconds
        if time.time() - self.start >= 1000:
            self.start = time.time()
            for _ in range(10):
                newalien = Alien(self.alien1_ch, self.alien2_ch, randint(0, self.engine_board_width), time.time())
                self.board.grid[newalien.pos] = newalien.ch1
                self.aliens.append(newalien)
                self.total_aliens += 1

    def run(self):

        def alarmhandler(signum, frame):
            raise TypeError

        def getinp(timeout=0.5):
            signal.signal(signal.SIGALRM, alarmhandler)
            signal.setitimer(signal.ITIMER_REAL, timeout)
            try:
                ch = self.getch()
                signal.alarm(0)
                return ch
            except TypeError:
                pass
            signal.signal(signal.SIGALRM, signal.SIG_IGN)
            return ''

        for _ in range(1):
            newalien = Alien(self.alien1_ch, self.alien2_ch, randint(0, int(self.board_array_size/4)), time.time())
            self.board.grid[newalien.pos] = newalien.ch1
            self.aliens.append(newalien)
            self.total_aliens += 1
            
        while True:

            self.board.render()
            status_bar = Bcolors.HEADER + "YOUR SCORE: " + Bcolors.OKBLUE + str(self.score) + Bcolors.OKCYAN + "\t MISSILES USED: " + Bcolors.WARNING + str(self.total_missiles) + Bcolors.OKGREEN + "\t ALIENS HIT: " + str(self.aliens_hit) + Bcolors.ENDC
            print(status_bar)
            #print 'Theoretical maximum score was: ', self.total_aliens * 10
            
            #Get user input
            inp = getinp()

            if (inp == 'a') or (inp == 'A') or (inp == '\x1b[C'):
                self.myShip.moveLeft()

            elif (inp == 'd') or (inp == 'D') or (inp == '\x1b[D'):
                self.myShip.moveRight()

            elif inp == ' ':
                newmissile = Missile('i', self.myShip.pos- self.engine_board_width, time.time(), 1, self.engine_board_height)
                self.board.grid[self.myShip.pos - self.engine_board_width] = newmissile.ch
                self.board.render()
                self.miss1.append(newmissile)
                self.score -= 5
                self.total_missiles += 1

            elif inp == 's':
                newmissile = Missile('l', self.myShip.pos-15, time.time(), 2, self.engine_board_height)
                self.board.grid[self.myShip.pos- self.engine_board_width] = 'l'
                self.board.render()
                self.miss2.append(newmissile)

            elif (inp == 'q') or (inp == 'Q'):
                print('Your total score was: ', self.score)
                print('Theoretical maximum score was: ', self.total_aliens * 10 - self.total_missiles * 5)
                print('Come back again!')

                print("self.spaceship_initial_position: ", self.spaceship_initial_position)
                print("self.engine_board_width: ", self.engine_board_width)
                print("self.engine_board_height: ", self.engine_board_height)
                print("self.board_array_size: ", self.board_array_size)
                print("int(self.engine_board_width/2): ", int(self.engine_board_width/2))
                print(Bcolors.WARNING + "Warning: No active frommets remain. Continue?" + Bcolors.ENDC) 
                exit("Bye")

            else:
                pass

            self.updateboard()
            
    
