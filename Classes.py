'''module containing various classes'''

import os

class _Getch:
     """Gets a single character from standard input.  Does not echo to the
 screen."""
     def __init__(self):
         try:
             self.impl = _GetchWindows()
         except ImportError:
             self.impl = _GetchUnix()

     def __call__(self): 
         return self.impl()


class _GetchUnix:
     def __init__(self):
         import tty, sys


     def __call__(self):
         import sys, tty, termios
         fd = sys.stdin.fileno()
         old_settings = termios.tcgetattr(fd)
         try:
             tty.setraw(sys.stdin.fileno())
             ch = sys.stdin.read(1)
         finally:
             termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
         return ch


class _GetchWindows:
     def __init__(self):
         import msvcrt

     def __call__(self):
         import msvcrt
         return msvcrt.getch()


class Debug:
    s1 = ''
    s2 = ''
    s3 = ''
    s4 = ''
    debug_str = ''
   
    def __init__(self):
        
        from datetime import datetime
        f = none

        #create file. Open for output
        f = open("debug.err","w+")

        # datetime object containing current date and time
        now = datetime.now()

        # dd/mm/YY H:M:S
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        f.write("Thus run began at: " % dt_string)

        
    def debug_string(s1 = "", s2 = "", s3 = "", s4 = ""):

        f = None

        if s1 != "":
            Debug.s1 = s1
        if s2 != "":
            Debug.s2 = s2
        if s3 != "":
            Debug.s3 = s3
        if s4 != "":
            Debug.s4 = s4
      
        # Open for output
        f = open("debug.err","a")
        #write
        debug_str = Debug.s1 + "\t" + Debug.s2 + "\t" + Debug.s3 + "\t" + Debug.s4 + "\n"
        f.write(debug_str)

    def debug_grid(grid, engine_board_height, engine_board_width):

        f = None
      
        # Open for output
        f = open("debug.err","a")
        
        
        for i in range(engine_board_height):
            #print("printRow:" , i , " " , self.grid[i * 20:i * 20 + 20])
            debug_str = str(grid[i * engine_board_height:i * engine_board_height + engine_board_width]) + '\n'
            f.write(debug_str)


class Bcolors:
    
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



class Dialog:
    
    '''creates an dialog box'''
    '''We want to create a self.engine_board_width x self.engine_board_height board'''

    def __init__(self, dialog_text):
        self.sentences = [] #Make an array for holding the sentences
        self.sentences = dialog_text.splitlines()
        
        
    @staticmethod
    def printBoundary():
        str = ''
        for i in range(1):
            str += ' '
        
        str_line = ''
        for i in range(76):
            str_line += '-'
            
        print((' {}' + str_line).format(str))
    

    @staticmethod
    def printRow(row_str):
        str = ''
        print
        for i in range(1):
            str += ' '
        
        print('{}|     {}'.format(str, row_str.ljust(72,' ')))
        
        
    def render(self):
        os.system('tput reset')
        Dialog.printBoundary()
        for i in range(len(self.sentences)):
            # Iterate through the sentences
            Dialog.printRow(self.sentences[i])
            
        Dialog.printBoundary()
        #print('\n\n')




class Board:
    

    '''creates an 15x15 board'''
    '''We want to create a self.engine_board_width x self.engine_board_height board'''

    def __init__(self, engine_board_width, engine_board_height):
        self.engine_board_width = engine_board_width
        self.engine_board_height = engine_board_height
        self.board_array_size = int(engine_board_width * engine_board_height)
        self.grid = [' '] * (self.engine_board_width * self.engine_board_height)
        #print("self.engine_board_width", self.engine_board_width)
        #print("self.engine_board_height", self.engine_board_height)
        
    def getGrid(self):
        return self.grid

    @staticmethod
    def printBoundary():
        #str = '            '
        str = ''
        for i in range(12):
            str += ' '
        print('{}---------------------------------------'.format(str))
    

    @staticmethod
    def printRow(r):
        str = '           '
        print('{}|{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}|'.format(
            str, r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16], r[17], r[18], r[19]))

#print(bcolors.WARNING + "Warning: No active frommets remain. Continue?" + bcolors.ENDC) 

    def render(self):
        os.system('tput reset')
        Board.printBoundary()
        for i in range(self.engine_board_height):
            #print("printRow:" , i , " " , self.grid[i * 20:i * 20 + 20])
            Board.printRow(self.grid[i * self.engine_board_height:i * self.engine_board_height + self.engine_board_width])
        Board.printBoundary()
        ##print('\n\n')
        
        
class Spaceship:
    '''cretes an instance of the spaceship'''

    def __init__(self, ch, pos, board, engine_board_width, engine_board_height):
        self.ch = ch
        self.pos = pos
        self.board = board
        self.board.grid[pos] = self.ch
        self.engine_board_width = engine_board_width
        self.engine_board_height = engine_board_height
        self.board_array_size = int(engine_board_width * engine_board_height)
        

    def moveLeft(self):
        '''moves the spaceship to the left'''
        if self.pos > (self.board_array_size - self.engine_board_width):
            self.board.grid[self.pos] = ' '
            self.board.grid[self.pos - 1] = self.ch
            self.pos = self.pos - 1
            self.board.render()

    def moveRight(self):
        '''moves the spaceship to the right'''
        if self.pos < self.board_array_size - 1 :
            self.board.grid[self.pos] = ' '
            self.board.grid[self.pos + 1] = self.ch
            self.pos = self.pos + 1
            self.board.render()


class Missile:
    '''creates two types of missiles, 
       one that moves two rows up and
       the other, one row up after certain
       time interval'''

    def __init__(self, ch, pos, time, jump, engine_board_height):
        self.ch = Bcolors.OKBLUE + ch + Bcolors.ENDC
        self.pos = pos
        self.time = time
        self.jump = engine_board_height * jump


class Alien:
    '''creates an alien represented
       by some special character'''

    def __init__(self, ch1, ch2, pos, time):
        self.ch1 = ch1
        self.ch2 = ch2
        self.pos = pos
        self.time = time


class Explosion:
    '''creates an explosiomn represented
       by some special character'''

    def __init__(self, ch1, pos, time):
        self.ch1 = ch1
        self.pos = pos
        self.time = time
    
