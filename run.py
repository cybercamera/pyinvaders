import SpaceInvaders
#from GetInput import _getChUnix as getinp
import os
#import shutil

#import termcolor

from Classes import _Getch
from Classes import Dialog
from Classes import Debug
from Classes import Bcolors


#creating an instance of the game engine
myEngine = SpaceInvaders.Engine()

#general instructions to play the game
os.system('tput reset')

#print('----------------- Welcome to Terminal Space Invaders --------------\n')
#print('Instructions:')
#print('Press "a" or "A" to move the spaceship to left.')
#print('Press "d" or "D" to move the spaceship to right.')
#print('Use SPACEBASR to shoot.')
#print('Press "q" or "Q" to quit.\n')
#print('Each alien you hit earns you 10 points.')
#print('Each missile costs you 5 points.')
#print('Each alien that escapes you costs you 5 points.')
#print('The aim of the game is to hit all of the aliens with the smallest number of missiles.')
#print('Your score will represent your success in this endeavour.')
#print('\nPress the ANY key to start the game!')

dialog_str = ''
dialog_str = dialog_str + '\n'
dialog_str = dialog_str + Bcolors.BOLD + Bcolors.WARNING + '----------------- Welcome to Terminal Space Invaders ---------------' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + '\n'
dialog_str = dialog_str + 'Instructions:\n'
dialog_str = dialog_str + '\n'
dialog_str = dialog_str + Bcolors.OKBLUE + 'Press "a" or "A" to move the spaceship to left.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKBLUE + 'Press "d" or "D" to move the spaceship to right.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKBLUE + 'Use SPACEBASR to shoot.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKBLUE + Bcolors.HEADER + 'Press "q" or "Q" to quit.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + '\n'
dialog_str = dialog_str + Bcolors.OKGREEN + 'Each alien you hit earns you 10 points.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKGREEN + 'Each missile costs you 5 points.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKGREEN + 'Each alien that escapes you costs you 5 points.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + '\n'
dialog_str = dialog_str + Bcolors.OKCYAN + Bcolors.BOLD + 'The aim of the game is to hit all of the aliens with the ' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKCYAN + Bcolors.BOLD + 'smallest number of missiles.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + Bcolors.OKCYAN + Bcolors.BOLD + 'Your score will represent your success in this endeavour.' + Bcolors.ENDC + '\n'
dialog_str = dialog_str + '\n'
dialog_str = dialog_str + Bcolors.UNDERLINE + Bcolors.FAIL + 'Press the ANY key to start the game!' + Bcolors.ENDC 

Debug.debug_string()

introDialog = Dialog(dialog_str)
introDialog.render()



#inp = getinp()
inp = _Getch()
if inp() != 'q':
    myEngine.run()
else:
    print('Come back again!')
